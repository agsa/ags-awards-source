//////////////////////////////////////////////////////////////////////////////////////////////////////////
// VOLUME CONTROL SCRIPT MODULE - Header
//
// This module is meant to help keep track of your volume settings (i.e. the in-game volume controls),
// and to store them to the profile (so they get loaded the next time you run the game). The initial plan
// was that it would also make it possible to adjust the volume of specific sound clips (so that the
// defaultVolume of the clip would be scaled according to the volume control), but that didn't work and I
// gave up on it. Some parts are therefore redundant.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Volume control data/API
struct Volume
{
  /// The name of this volume data (used as the property name in the profile file)
  String name;
  /// The audio type this volume controls
  AudioType type;
  /// The current volume setting (ignoring mute)
  int volume;
  /// Whether this volume is currently muted
  bool mute;

  /// The actual volume to play at (i.e., 0 if muted, otherwise volume)
  import int GetEffectiveVolume();
  /// Load settings for this volume type from the profile
  import bool LoadFromProfile();
  /// Save settings for this volume type to the profile
  import void SaveToProfile();
  /// Actually set the game volume setting to these values
  import void Apply();
  /// Initialize the volume API, creating the default volume instances
  import static void Init();
};

// Play an audio clip using the volume settings here. Deprecated because it didn't work
// import AudioChannel* PlayVolume(this AudioClip*, AudioPriority audioPriority = eAudioPriorityNormal, RepeatStyle repeatStyle = eOnce);

/// Volume control settings for sounds
import Volume soundVol;
/// Volume control settings for music
import Volume musicVol;