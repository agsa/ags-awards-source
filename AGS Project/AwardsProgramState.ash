//////////////////////////////////////////////////////////////////////////////////////////////////////////
// AWARDS PROGRAM STATE SCRIPT MODULE - Header
//
// The AwardsProgramState struct is meant to hold all the persistent state of the ceremony that is
// broadcast to all participants (which category we're in, which game is being announced, etc.).
// It is encoded into and communicated via the IRC CAFE_CHANNEL topic by the "director", and decoded and
// updated accordingly by all the participants' clients.
//
// This is what determines what gets shown on screen, what the topic texts are, etc. It's how the
// director controls the flow of the ceremony (the logic for that is in Director.asc).
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// A value that indicates no music is playing
#define MUSIC_STOP    -1
/// A value that indicates the screen is off
#define SCREEN_OFF    -1

/// A stage or phase in the announcement of each category
enum CategoryStage
{
  // Opening intro of the category
  eOpeningIntro, 
  /// The category is being introduced
  eIntro,
  /// The nominees are being announced
  eNominees,
  /// The nominees have been announced, and we're waiting for the winner
  eDrumRoll,
  /// The winner is being announced, and a fanfare is being played
  eFanfare, 
  /// The winner is announced, and may be giving acceptance speech
  eAnnounced
};

enum Trophy
{
  eTrophyNone = 0, 
  eTrophyBestGame, 
  eTrophyMaggiesBronze, 
  eTrophyMaggiesSilver, 
  eTrophyMaggiesGold
};

/// API for setting and checking the state of the Awards program
struct AwardsProgramState
{
  /// Find the winner for a certain category (-1 if not yet announced)
  import static AwardsNomineePtr GetWinner(AwardsCategoryPtr category);
  /// Used by the director to set the winner of a category
  import static bool SetWinner(AwardsCategoryPtr category,  AwardsNomineePtr winner);

  /// Find the category that is currently being announced
  import static AwardsCategoryPtr GetCurrentCategory();
  /// Used by the director to set the category currently being announced
  import static bool SetCurrentCategory(AwardsCategoryPtr category);

  /// Find the current stage/phase of the category being announced
  import static CategoryStage GetCurrentStage();
  /// Used by the director to set the stage of the category being announced
  import static void SetCurrentStage(CategoryStage stage);

  /// Find the nominee currently being announced in the current category
  import static AwardsNomineePtr GetCurrentNominee();
  /// Used by the director to set the nominee being announced in the current category
  import static bool SetCurrentNominee(AwardsNomineePtr nominee);

  /// Check whether the on-stage screen is currently displaying one of the VANIMATEDGIFS animations, or a static sprite (this influences how to interpret the GetCurrentScreen() value
  import static bool IsScreenViewAnimation();

  /// Find what's currently being displayed on the on-stage screen, either as a sprite index or as a loop within the VANIMATEDGIFS view
  import static int GetCurrentScreen();
  /// Used by the director to display a particular sprite on the on-stage screen
  import static void SetCurrentScreen(int sprIndex);
  /// Used by the director to display a particular VANIMATEDGIFS loop as an animation on the on-stage screen
  import static void SetCurrentScreenAnimation(int loopIndex);

  /// Find what music is currently playing (if set specifically, not by an awards category)
  import static int GetCurrentMusic();
  /// Used by the director to set the music currently playing (potentially overriding any default music in the category)
  import static void SetCurrentMusic(int music);

  /// Find what mode the lights are currently set to
  import static LightMode GetCurrentLightMode();
  /// Used by the director to set the lights to a particular mode (potentially overriding the default or mode set by the category)
  import static void SetCurrentLightMode(LightMode mode);
  
  import static Trophy GetCurrentTrophy();
  import static void SetCurrentTrophy(Trophy trophy);
  
  import static bool GetCurrentLectern();
  import static void SetCurrentLectern(bool present);

  /// Find what text is currently displayed on the first line above stage (if set specifically, not by an awards category)
  import static String GetTitleText();
  /// Used by the director to set the text displayed on the first line above stage (potentially overriding any text set by the category)
  import static void SetTitleText(String titleText);
  /// Find what text is currently displayed on the second line above stage (if set specifically, not by an awards category)
  import static String GetDescriptionText();
  /// Used by the director to set the text displayed on the second line above stage (potentially overriding any text set by the category)
  import static void SetDescriptionText(String descriptionText);

  /// Find the current "stifle" level of the applause (a bias to how many people need to clap before the applause sounds)
  import static int GetApplauseStifleLevel();
  /// Used by the director to set the "stifle" level of the applause
  import static void SetApplauseStifleLevel(int clapStifleLevel);
  /// Find the current "stifle" level of laughter (a bias to how many people need to laugh before the laughter sounds) - currently unused
  import static int GetLaughStifleLevel();
  /// Used by the director to set the "stifle" level of the laughter - currently unused
  import static void SetLaughStifleLevel(int lolStifleLevel);
  /// Find the current "stifle" level of crowd murmur (a bias to how many people need to be speaking before the crowd murmur sounds) - currently unused
  import static int GetMurmurStifleLevel();
  /// Used by the director to set the "stifle" level of the crowd murmur - currently unused
  import static void SetMurmurStifleLevel(int rhubarbStifleLevel);

  /// Initialize the awards program state (set all values to default) - internal use
  import static void Init();
  /// Load an awards program state value from a key-value pair section of an encoded string - internal use
  import static bool ReadKeyValue(String key,  String value);
  /// Load the awards program state from an encoded string by parsing it
  import static bool ReadFromString(String encodedString);
  /// Used by the director to encode the current awards program state as a string, to be set as the IRC CAFE_CHANNEL topic
  import static String EncodeToString();

  /// Checks whether the current category was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedCategory();
  /// Checks whether the current stage was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedStage();
  /// Checks whether the current nominee was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedNominee();
  /// Checks whether the current screen was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedScreen();
  /// Checks whether the current music was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedMusic();
  /// Checks whether the current light mode was updated when the awards program state was last updated with ReadFromString()
  import static bool UpdatedLights();
  
  import static bool UpdatedTrophy();
  import static bool UpdatedLectern();

  /// Checks whether the on-stage screen is currently being overridden
  import static bool IsScreenOverride();
  /// Used by the director to override the on-stage screen (to show something else than the default screen during a category, for example)
  import static void SetScreenOverride(bool override);
  /// Checks whether the music is currently being overridden
  import static bool IsMusicOverride();
  /// Used by the director to override the music (to play something different than the default music during a category, for example)
  import static void SetMusicOverride(bool override);
  /// Checks whether the on-stage screen is currently being overridden
  import static bool IsRoomStateOverride();
  /// Used by the director to override the on-stage screen (to show something else than the default screen during a category, for example)
  import static void SetRoomStateOverride(bool override);
  /// Checks whether the text (title and description) is currently being overridden
  import static bool IsTextOverride();
  /// Used by the director to override the text
  import static void SetTextOverride(bool override);

  /// Used by the director to clear all overrides
  import static void ClearOverrides();
};

// To broadcast the Awards Program State, it is encoded as a String containing a semicolon-separated list
// of key-value pairs, with the keys and values separated by commas:
// 1,2;3,4;m,4;i,124;c,4;p,3 ...
//
// GUIDE TO THE MEANING OF THE KEYS
//
// Numbers: winner for that category (key is AwardsCategoryPtr and value is AwardsNomineePtr)
//
// CATEGORY PRESENTATION (AUTO):
// c: current category
// p: current category stage (phase)
// n: current nominee
//
// MANUAL CONTROL:
// g: current screen graphic (sprite index)
// a: current screen animation (loop index in vAnimatedGifs)
// m: current music
// t: title text
// d: description text
// i: light setting
//
// STIFLE LEVELS
// k: clap stifle
// l: laughter stifle
// w: murmur/whisper stifle