AGSScriptModule    Ferry "Wyz" Timmers A IRC (internet relayed chat) client.  IRC module alpha 1 Q;  
Socket *server;

bool connected = false;
String hostname;
String nickname;

String ircFrom;
String ircTo;
String ircAction;
String ircMessage;
export ircFrom, ircTo, ircAction, ircMessage;

String buffer;
String names;

//--------------------------------------------------------------------------------

String Left(this String *, int i)
{
  if (i <= 0)
    return ("");
  else
    return (this.Truncate(i));
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String Right(this String *, int i)
{
  if (i >= this.Length)
    return ("");
  else
    return (this.Substring(i, this.Length - i));
}

//--------------------------------------------------------------------------------

String IRCCommand::Pack()
{
  String command = "";
  
  if (String.IsNullOrEmpty(this.name))
    return (command);
  
  if (!String.IsNullOrEmpty(this.sign))
    command = String.Format(":%s ", this.sign);
  
  command = command.Append(this.name);
  
  int i = 0;
  while ((i < IRC_MAX_ARGS) && (!String.IsNullOrEmpty(this.args[i])))
  {
    command = command.AppendChar(' ');
    command = command.Append(this.args[i]);
    i++;
  }
  
  if (!String.IsNullOrEmpty(this.msg))
    command = command.Append(String.Format(" :%s", this.msg));
  
  return (command);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCCommand::Unpack(String command)
{
  int pos;
  
  if (command.StartsWith(":"))
  {
    pos = command.IndexOf(" ");
    if (pos < 0)
    {
      this.sign = command.Substring(1, command.Length - 1);
      command = "";
    }
    else
    {
      this.sign = command.Substring(1, pos - 1);
      command = command.Right(pos + 1);
    }
  }
  
  pos = command.IndexOf(" :");
  if (pos >= 0)
  {
    this.msg = command.Right(pos + 2);
    command = command.Left(pos);
  }
  
  pos = command.IndexOf(" ");
  if (pos < 0)
  {
    this.name = command.UpperCase();
    command = "";
  }
  else
  {
    this.name = command.Left(pos);
    this.name = this.name.UpperCase();
    command = command.Right(pos + 1);
  }
  
  int i = 0;
  pos = command.IndexOf(" ");
  while ((pos >= 0) && (i < IRC_MAX_ARGS))
  {
    this.args[i] = command.Left(pos);
    command = command.Right(pos + 1);
    
    pos = command.IndexOf(" ");
    i++;
  }
  
  if (i < IRC_MAX_ARGS)
  {
    this.args[i] = command;
    i++;
    
    if (i < IRC_MAX_ARGS)
    {
      this.args[i] = null;
      i++;
    }
  }
}

//--------------------------------------------------------------------------------

String IRCUsersign::Pack()
{
  String sign = "";
  
  if (!String.IsNullOrEmpty(this.nick))
    sign = this.nick;
  
  if (!String.IsNullOrEmpty(this.user))
  {
    sign = sign.AppendChar('!');
    sign = sign.Append(this.user);
  }
  
  if (!String.IsNullOrEmpty(this.host))
  {
    sign = sign.AppendChar('@');
    sign = sign.Append(this.host);
  }
  
  return (sign);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCUsersign::Unpack(String sign)
{
  int pos;
  
  pos = sign.IndexOf("@");
  if (pos >= 0)
  {
    this.host = sign.Right(pos + 1);
    sign = sign.Left(pos);
  }
  
  pos = sign.IndexOf("!");
  if (pos >= 0)
  {
    this.user = sign.Right(pos + 1);
    sign = sign.Left(pos);
  }
  
  this.nick = sign;
}

//--------------------------------------------------------------------------------

void IRCRaw(String command)
{
  if ((server == null) || !connected)
    return;
  
  server.Send(command.Append("\r\n"));  
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCDisconnect()
{
  IRCCommand cmd;
  cmd.name = "QUIT";
  cmd.msg = "Client disconnected.";
  IRCRaw(cmd.Pack());
  
  if (server != null)
    server.Close();
  
  connected = false;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

bool IRCConnect(String address, int port)
{
  if (connected)
    IRCDisconnect();
  
  SockAddr *ip = SockAddr.CreateIP(address, port);
  server = Socket.CreateTCP();
  connected = !server.Connect(ip);
  server.blocking = false;
  hostname = address;
  buffer = "";
  names = null;
  return (connected);
}

//--------------------------------------------------------------------------------

void IRCNick(String nick)
{
  IRCCommand cmd;
  cmd.name = "NICK";
  cmd.args[0] = nick;
  IRCRaw(cmd.Pack());
  nickname = nick;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCUser(String email, String realname)
{
  IRCCommand cmd;
  cmd.name = "USER";
  cmd.args[0] = email;
  cmd.args[1] = "8";
  cmd.args[2] = "*";
  cmd.msg = realname;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCJoin(String channel, String key)
{
  IRCCommand cmd;
  cmd.name = "JOIN";
  cmd.args[0] = channel;
  cmd.args[1] = key;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCPart(String channel, String message)
{
  IRCCommand cmd;
  cmd.name = "PART";
  cmd.args[0] = channel;
  cmd.msg = message;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCMode(String to, String flags, String parameter)
{
  IRCCommand cmd;
  cmd.name = "MODE";
  cmd.args[0] = to;
  cmd.args[1] = flags;
  cmd.args[2] = parameter;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCTopic(String channel, String topic)
{
  IRCCommand cmd;
  cmd.name = "TOPIC";
  cmd.args[0] = channel;
  cmd.args[1] = topic;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCNames(String channel)
{
  IRCCommand cmd;
  cmd.name = "NAMES";
  cmd.args[0] = channel;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCList(String channel)
{
  IRCCommand cmd;
  cmd.name = "LIST";
  cmd.args[0] = channel;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCInvite(String channel, String nick)
{
  IRCCommand cmd;
  cmd.name = "INVITE";
  cmd.args[0] = nick;
  cmd.args[1] = channel;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCKick(String channel, String nick, String message)
{
  IRCCommand cmd;
  cmd.name = "KICK";
  cmd.args[0] = channel;
  cmd.args[1] = nick;
  cmd.msg = message;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCMsg(String to, String message)
{
  IRCCommand cmd;
  cmd.name = "PRIVMSG";
  cmd.args[0] = to;
  cmd.msg = message;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCNotice(String to, String message)
{
  IRCCommand cmd;
  cmd.name = "NOTICE";
  cmd.args[0] = to;
  cmd.msg = message;
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCCtcp(String to, String name, String message)
{
  IRCCommand cmd;
  cmd.name = "PRIVMSG";
  cmd.args[0] = to;
  if (String.IsNullOrEmpty(message))
    cmd.msg = String.Format("%c%s%c", 1, name.UpperCase(), 1);
  else
    cmd.msg = String.Format("%c%s %s%c", 1, name.UpperCase(), message, 1);
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCCtcpReply(String to, String name, String message)
{
  IRCCommand cmd;
  cmd.name = "NOTICE";
  cmd.args[0] = to;
  if (String.IsNullOrEmpty(message))
    cmd.msg = String.Format("%c%s%c", 1, name.UpperCase(), 1);
  else
    cmd.msg = String.Format("%c%s %s%c", 1, name.UpperCase(), message, 1);
  IRCRaw(cmd.Pack());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCAction(String to, String message)
{
  IRCCtcp(to, "ACTION", message);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void IRCPong(String response)
{
  IRCCommand cmd;
  cmd.name = "PONG";
  cmd.msg = response;
  IRCRaw(cmd.Pack());
}

//--------------------------------------------------------------------------------

function repeatedly_execute()
{
  if ((server == null) || (!connected))
    return;
  
  String str = server.Recv();
  while (str != null)
  {
    buffer = buffer.Append(str);
    str = server.Recv();
  }
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String IRCReceiveRaw(){
  if (!connected)
    return (null);
  
  String command = null;
  
  int pos = buffer.IndexOf("\r\n");
  if (pos >= 0)
  {
    command = buffer.Left(pos);
    buffer = buffer.Right(pos + 2);
  }
  
  return (command);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

IRCEventType IRCProcess(String command)
{
  if (command == null)
    return (eIRCNone);
  
  ircFrom = "";
  ircTo = "";
  ircAction = "";
  ircMessage = "";
  
  IRCCommand cmd;
  cmd.Unpack(command);
  int code = cmd.name.AsInt;
  
  if (!String.IsNullOrEmpty(cmd.sign))
  {
    IRCUsersign sign;
    sign.Unpack(cmd.sign);
    ircFrom = sign.nick;
  }
  
  if (cmd.name == "PING")
    IRCPong(cmd.msg);
  else if (cmd.name == "PRIVMSG")
  {
    ircTo = cmd.args[0];
    
    if ((cmd.msg.Chars[0] == 1) && (cmd.msg.Chars[cmd.msg.Length - 1] == 1)) /* CTCP */
    {
      String ctcp_name, ctcp_msg;
      int pos = cmd.msg.IndexOf(" ");
      if (pos >= 0)
      {
        ctcp_name = cmd.msg.Substring(1, pos - 1);
        ctcp_msg = cmd.msg.Substring(pos + 1, cmd.msg.Length - pos - 2);
      }
      else
      {
        ctcp_name = cmd.msg.Substring(1, cmd.msg.Length - 2);
        ctcp_msg = "";
      }
      
      if (ctcp_name == "ACTION")
      {
        ircMessage = ctcp_msg;
        return (eIRCAction);
      }
      else if (ctcp_name == "VERSION")
        IRCCtcpReply(ircFrom, "VERSION", String.Format("agsIRC alpha1 using AGS v%s", System.Version));
      else if (ctcp_name == "PING")
        IRCNotice(ircFrom, cmd.msg);
      else if (ctcp_name == "TIME")
      {
        DateTime *dt = DateTime.Now;
        IRCCtcpReply(ircFrom, "TIME", String.Format("%d/%d/%d %d:%d:%d",
          dt.Month, dt.DayOfMonth, dt.Year, dt.Hour, dt.Minute, dt.Second));
      }
      else
      {
        ircAction = ctcp_name;
        ircMessage = ctcp_msg;
        return (eIRCCtcp);
      }
    }
    else /* no CTCP */
    {
      ircMessage = cmd.msg;
      return (eIRCMsg);
    }
  }
  else if (cmd.name == "NOTICE")
  {
    ircTo = cmd.args[0];
    
    if ((cmd.msg.Chars[0] == 1) && (cmd.msg.Chars[cmd.msg.Length - 1] == 1)) /* CTCP */
    {
      String ctcp_name, ctcp_msg;
      int pos = cmd.msg.IndexOf(" ");
      if (pos >= 0)
      {
        ctcp_name = cmd.msg.Substring(1, pos - 1);
        ctcp_msg = cmd.msg.Substring(pos + 1, cmd.msg.Length - pos - 2);
      }
      else
      {
        ctcp_name = cmd.msg.Substring(1, cmd.msg.Length - 2);
        ctcp_msg = "";
      }
      
      ircAction = ctcp_name;
      ircMessage = ctcp_msg;
      return (eIRCCtcpReply);
    }
    else
    {
      ircMessage = cmd.msg;
      return (eIRCNotice);
    }
  }
  else if (cmd.name == "JOIN")
  {
    if (cmd.msg)
      ircTo = cmd.msg;
    else
      ircTo = cmd.args[0];
    return (eIRCJoin);
  }
  else if (cmd.name == "PART")
  {
    ircTo = cmd.args[0];
    ircMessage = cmd.msg;
    return (eIRCPart);
  }
  else if (cmd.name == "NICK")
  {
    ircMessage = cmd.msg;
    ircAction = cmd.args[0];
    if (String.IsNullOrEmpty(ircAction))
      ircAction = ircMessage;
    return (eIRCNick);
  }
  else if (cmd.name == "QUIT")
  {
    ircMessage = cmd.msg;
    return (eIRCQuit);
  }
  else if (cmd.name == "MODE")  {
    ircTo = cmd.args[0];
    ircAction = cmd.args[1];
    ircMessage = cmd.args[2];
    return (eIRCMode);
  }
  else if (cmd.name == "TOPIC")  {
    ircTo = cmd.args[0];
    ircMessage = cmd.msg;
    return (eIRCTopic);
  }
  else if (cmd.name == "INVITE")
  {
    ircTo = cmd.args[1];
    return (eIRCInvite);
  }
  else if (cmd.name == "KICK")
  {
    ircTo = cmd.args[0];
    ircAction = cmd.args[1];
    ircMessage = cmd.msg;
    return (eIRCKick);
  }
  else if (code == 1)
  {
    ircMessage = cmd.msg;
    return (eIRCWelcome);
  }
  
  	else if (code == 332) {
	ircTo = cmd.args[1];
	ircMessage = cmd.msg;
	return (eIRCTopic);
	}  
  else if (code == 324) {
	ircTo = cmd.args[1];
  ircAction = cmd.args[2];
  ircMessage = cmd.args[3];
	return (eIRCMode);
	}  
  else if (code == 322)  {
    ircTo = cmd.args[0];
    ircAction = cmd.args[1];
    ircMessage = cmd.msg;
    return (eIRCList);
  }
  else if (code == 323)
  {
    return (eIRCListEnd);
  }
  else if (code == 353)
  {
    if (names == null)
      names = cmd.msg;
    else
    {
      names = names.AppendChar(' ');
      names = names.Append(cmd.msg);
    }
  }
  else if (code == 366)
  {
    ircTo = cmd.args[1];
    ircMessage = names;
    names = null;
    return (eIRCNames);
  }
  else if (code == 422)
  {
    // No message of the day, ignore this error
  }
  else if ((cmd.name == "ERROR") || ((code >= 400) && (code < 600)))
  {
    ircAction = cmd.name;
    ircMessage = cmd.msg;
    return (eIRCError);
  }
  
  return (eIRCNext);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

IRCEventType IRCReceive()
{
  IRCEventType type = IRCProcess(IRCReceiveRaw());
  
  while (type == eIRCNext)
    type = IRCProcess(IRCReceiveRaw());
  
  return (type);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String []IRCSplit(this String *)
{
  int count = 1;
  int length = this.Length;
  int i;
  int j;
  
  if (length)
  {
    length--;
    while (this.Chars[length] == ' ') length--;
    length++;
  }
  
  i = length;
  while (i)
  {
    i--;
    if (this.Chars[i] == ' ')
      count++;
  }
  
  String list[] = new String[count + 1];
  
  count = 0;
  i = 0;
  j = 0;
  while (i < length)
  {
    if (this.Chars[i] == ' ')
    {
      list[count] = this.Substring(j, i - j);
      j = i + 1;
      count++;
    }
    i++;
  }
  list[count] = this.Substring(j, length - j);
  count++;
  
  list[count] = null;
  return (list);
}

//--------------------------------------------------------------------------------
 �  /******************************************************************************
 * IRC module for AGS                                                         *
 *    v alpha1                                                                *
 *                                                                            *
 * Author: Ferry "Wyz" Timmers                                                *
 *                                                                            *
 * Date: November 2010                                                        *
 *                                                                            *
 * Dependencies: agssock.dll (v alpha 1)                                      *
 *                                                                            *
 * Description: A IRC (internet relayed chat) client.                         *
 *                                                                            *
 ******************************************************************************
 

/*----------------------------------------------------------------------------*\
                              Connection
\*----------------------------------------------------------------------------*/

/// Opens a connection to a irc server, returns true on success
import bool IRCConnect(String address, int port = 6667);
/// Closes the opened irc connection
import void IRCDisconnect();

/// Sends a raw irc command. (You usually don't need this)
import void IRCRaw(String command);

/// Returns the next IRCCommand from the input buffer or null if empty. (You usually don't need this)
import String IRCReceiveRaw();
// See below (irc events) for IRCReceive();

/*----------------------------------------------------------------------------*\
                              Registration
\*----------------------------------------------------------------------------*/

/// Sets or changes the nick name.
import void IRCNick(String nick);
/// Sends user information to irc server as part of de identification.
import void IRCUser(String email, String realname);

/*----------------------------------------------------------------------------*\
                              Channel operations
\*----------------------------------------------------------------------------*/

/// Joins an irc channel (use key when channel is protected by a password)
import void IRCJoin(String channel, String key = 0);
/// Parts an irc channel
import void IRCPart(String channel, String message = 0);

/// Sets a user or channel mode.
import void IRCMode(String to, String flags, String parameter = 0);
/// Checks, sets or clears a channel topic.
import void IRCTopic(String channel, String topic = 0);
/// Lists the users that are on a certain channel.
import void IRCNames(String channel);
/// Lists the channel information of a certain channel.
import void IRCList(String channel);

/// Invites a user to a channel.
import void IRCInvite(String channel, String nick);
/// Kicks a user from a channel.
import void IRCKick(String channel, String nick, String message = 0);

/*----------------------------------------------------------------------------*\
                              Sending messages
\*----------------------------------------------------------------------------*/

/// Sends a chat message to either a channel or an user privately.
import void IRCMsg(String to, String message);
/// Sends a notice to either a channel or a user privately.
import void IRCNotice(String to, String message);
/// Sends an action to either a channel or a user privately.
import void IRCAction(String to, String message);

// Sends a custom client-to-client-protocol message to either a channel or a user privately.
import void IRCCtcp(String to, String name, String message = 0);
// Sends a custom client-to-client-protocol reply to either a channel or a user privately.
import void IRCCtcpReply(String to, String name, String message = 0);

/*----------------------------------------------------------------------------*\
                              IRC events (receiving)
\*----------------------------------------------------------------------------*/

// Event variables
// *NOTE*: These are globally defined so fetch them directly after calling IRCReceive();

import String ircFrom; // Nick name of the user that caused the event to happend
import String ircTo;   // Channel / user where the event was targetted (if any)

// See eventtype definition for information about these:
import String ircAction;
import String ircMessage;

enum IRCEventType
{
  eIRCNone,
  eIRCNext,
  
  /* User / channel events */
  eIRCNick,   // message = new name
  eIRCQuit,   // message = quit message from user
  
  eIRCJoin,
  eIRCPart,   // message = part message from user
  eIRCMode,   // action = flags, message = parameter
  eIRCTopic,  // message = topic
  eIRCInvite,
  eIRCKick,   // action = nick of kicked user, message = kick message from user
  
  eIRCMsg,    // message = chat message
  eIRCNotice, // message = notification
  eIRCAction, // message = action description
  eIRCCtcp,   // action = ctcp command, message = ctcp arguments
  eIRCCtcpReply,
  // name = ctcp command, message = ctcp arguments
  // *NOTE*: Do not send any message in return on ctcp replies, this will create a loop!
  
  /* Server replies */
  eIRCWelcome,// message = welcome message
  eIRCList,   // action = amount of visble users, message = topic
  eIRCListEnd,
  eIRCNames,  // message = list of nick names seperated by spaces (use message.IRCSplit())
  eIRCError,  // action = error code (see RFC 2812), message = readable error message
};

/// Returns the next Event from the input buffer. (also handles some events automatically)
import IRCEventType IRCReceive();
/// Returns the next Event from the specified string. (You usually don't need this)
import IRCEventType IRCProcess(String command);

/// Returns a IRC tokenized list as a dynamic String array. (last element <=> null)
import String []IRCSplit(this String *);

/*----------------------------------------------------------------------------*\
                              IRC message processing structs
                              (You usually don't need this)
\*----------------------------------------------------------------------------*/

#define IRC_MAX_ARGS 64

struct IRCCommand
{
  String sign;
  String name;
  String args[IRC_MAX_ARGS];
  String msg;

/// Returns the contents of the struct as a string. (See RFC 1459)
  import String Pack();
/// Sets the contents of the struct by string. (See RFC 1459)
  import void Unpack(String packed);
};

struct IRCUsersign
{
  String nick;
  String user;
  String host;

/// Returns the contents of the struct as a string. (See RFC 1459)
  import String Pack();
/// Sets the contents of the struct by string. (See RFC 1459)
  import void Unpack(String packed);
}; ��E        ej��