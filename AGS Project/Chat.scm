AGSScriptModule    Ferry "Wyz" Timmers  f  AV  bool connected = false;
String nickname, channel, metachan;
bool silence = false;


bool IsContained(String goeshere, String thatiscontained) {
  bool check;
  if (goeshere.IndexOf(thatiscontained)==-1) check=false;
  if (goeshere.IndexOf(thatiscontained)!=-1) check=true;
  return check;
}


bool GetSound(String goeshere, String tobecontained) {
bool check=false;
if (IsContained(goeshere, tobecontained)) check=true;
return  check;
}

function GetPicture(String goeshere) {
  String searches[30];
  
  searches[1]="Eternally Us";
  searches[2]="Snakes";
  searches[3]="Avalon";
  searches[4]="Technobabylon"; 
  searches[5]="By the Numbers";
  searches[6]="Hard Space";
  searches[7]="Kuma"; 
  searches[8]="Heels";
  searches[9]="rein";
  searches[10]="Blockz";
  searches[11]="Crystal Cursors";
  searches[12]="cities of gold";
  searches[13]="Matt to the Future";
  searches[14]="Quantumnauts";
  searches[15]="1st Drop";
  searches[16]="Barn Runner";
  searches[17]="Infinity bit";
  searches[18]="FORKLIFT";
  searches[19]="Space Pool Alpha";
  searches[20]="Professor Neely"; 
  searches[21]="Death";
  searches[22]="Earl Bobby";
  searches[23]="A woman for all seasons";
  searches[24]="Ben Jordan";
  searches[25]="OSD";
  searches[26]="Oceanspirit";
  searches[27]="Journey Down";
  
  int n=1;
  while (n!= 28) {
    if (IsContained(goeshere, searches[n])) {//sentence has eternally us
      return n+37;
    }
    n++;
  }
  //return 0;
}

void PushLine(String line){
  if (line.Length > LINE_WIDTH) {
    PushLine(line.Truncate(LINE_WIDTH));
    PushLine(line.Substring(LINE_WIDTH, line.Length - LINE_WIDTH));
    return;
  }  

  if (lstbox.ItemCount==200) {
    File *output;
    if (File.Exists("log.txt")) output = File.Open("log.txt", eFileAppend);
    if (!File.Exists("log.txt")) output = File.Open("log.txt", eFileWrite);
    
    DateTime *dt = DateTime.Now;
    String TempS = String.Format("%d/%d/%d||%d:%d:%d", dt.DayOfMonth, dt.Month, dt.Year, dt.Hour, dt.Minute, dt.Second);
    output.WriteRawLine(String.Format("%s     %s",TempS,lstbox.Items[0]));
    output.Close();
    lstbox.RemoveItem(0); 
  }
  
  lstbox.AddItem(line);
  lstbox.ScrollDown();
  if (lstbox.ItemCount>=lstbox.RowCount) lstbox.ScrollDown();
}

//--------------------------------------------------------------------------------

void _SortArray(String array[], int left, int right, String temp[])
{
  int length = right - left;
  int mid = left + (length / 2);
  
  if (length > 2)
  {
    _SortArray(array, left, mid, temp);
    _SortArray(array, mid, right, temp);
  }
  else
  {
    if ((length == 2) && (array[left].CompareTo(array[mid]) >= 0))
    {
      String str = array[left];
      array[left] = array[mid];
      array[mid] = str;
    }
    return;
  }
  
  int i, j, k;
  
  i = left;
  j = right;
  k = 0;
  while (i < j)
  {
    temp[k] = array[i];
    k++;
    i++;
  }
  
  mid -= left;
  i = 0;
  j = mid;
  k = left;
  while ((i < mid) || (j < length))  {
    if ((i < mid) && (j < length))    {
      if (temp[i].CompareTo(temp[j]) < 0)      {
        array[k] = temp[i];
        i++;
        k++;
      }
      else      {
        array[k] = temp[j];
        j++;
        k++;
      }
    }
    else if (i >= mid)    {
      array[k] = temp[j];
      j++;
      k++;
    }
    else if (j >= length)    {
      array[k] = temp[i];
      i++;
      k++;
    }
  }
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void SortArray(String array[]) {
  int right = 0;
  while (array[right] != null) right++;
  _SortArray(array, 0, right, new String[right]);
}

//--------------------------------------------------------------------------------

String GetNick(this String *)
{
  if ((this.Chars[0] < '0') || (this.Chars[0] > '}')
  || ((this.Chars[0] > '9') && (this.Chars[0] < 'A')))
    return (this.Substring(1, this.Length - 1));
  else
    return (this.Copy());
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String GetMode(this String *)
{
  if ((this.Chars[0] < '0') || (this.Chars[0] > '}')
  || ((this.Chars[0] > '9') && (this.Chars[0] < 'A')))
    return (this.Truncate(1));
  else
    return ("");
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void SetNicks(this ListBox *, String nicks[]) {
  this.Clear();
  SortArray(nicks);
  int i = 0;
  while (nicks[i] != null) {
    this.AddItem(nicks[i]);
    i++;
  }
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void AddNick(this ListBox *, String nick)
{
  int i = 0;
  while (i < this.ItemCount)  {
    if (this.Items[i].CompareTo(nick) > 0)    {
      this.InsertItemAt(i, nick);
      return;
    }
    i++;
  }
  this.AddItem(nick);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String CheckMode(this ListBox *, String nick) {
  nick = nick.GetNick();
  
  int i = this.ItemCount;
  while (i) {
    i--;
    
    String item = this.Items[i];
    
    if (nick == item.GetNick())
      return (item.GetMode());
  }		
  
  return ("");
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

String RemoveNick(this ListBox *, String nick){
  nick = nick.GetNick();
  
  int i = this.ItemCount;
  while (i)  {
    i--;
    
    String item = this.Items[i];
    
    if (nick == item.GetNick())
    {
      this.RemoveItem(i);
      return (item.GetMode());
    }
  }
  
  return ("");
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ChangeNick(this ListBox *, String nick, String newnick)
{
  String mode = this.RemoveNick(nick);
  this.AddNick(mode.Append(newnick));
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ChangeMode(this ListBox *, String nick, String mode)
{
  this.RemoveNick(nick);
  this.AddNick(mode.Append(nick));
}

//--------------------------------------------------------------------------------

NPC npc[MAX_NPC];
export npc;

int npc_count = 0;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void NPC::ChangeNick(String nick)
{
  if ((nick.Chars[0] < '0') || (nick.Chars[0] > '}')
  || ((nick.Chars[0] > '9') && (nick.Chars[0] < 'A')))
  {
    this.mode = nick.Chars[0];
    this.nick = nick.Substring(1, nick.Length - 1);
  }
  else
  {
    this.mode = 0;
    this.nick = nick;
  }
  this.body.Name = this.nick;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void NPC::Leave()
{
  if ((this.id < 0) || (this.id >= npc_count))
    return;
  
  // Reset body
  this.body.StopMoving();
  this.body.y = 800;
  this.body.Name = "";
  this.body.ChangeView(8);
  this.body.SpeechColor=64170;
    
  npc_count--;
  this.nick = npc[npc_count].nick;
  this.mode = npc[npc_count].mode;
  this.body = npc[npc_count].body;
  this.virtual = npc[npc_count].virtual;
  
  // ... the rest
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

static int NPC::Count()
{
  return (npc_count);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

static int NPC::Find(String nick)
{
  // Note: for future implementations use bin search
  nick = nick.GetNick();
  
  int i = npc_count;
  while (i)
  {
    i--;
    if (npc[i].nick == nick)
      return (i);
  }
  return (-1);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

static int NPC::Create(String nick, bool virtual){
  nick = nick.GetNick();
  
  if (game.debug_mode==1)  PushLine(String.Format("Created: (%s) virtual=%d", nick,  virtual));
  
  int i;
  Character *body;
  
  i = NPC.Find(nick);
  if (i >= 0)
  {
    if (virtual == false)
      npc[i].virtual = false;
    return (i);
  }
  
  i = Game.CharacterCount;
  while (i > 1)  {
    i--;
    if (String.IsNullOrEmpty(character[i].Name))
      body = character[i];
  }
  
  if (body == null)
    return;
  npc[npc_count].body = body;
  npc[npc_count].body.StopMoving();
  npc[npc_count].body.x=629;
  npc[npc_count].body.y=312;
  npc[npc_count].id = npc_count;
  npc[npc_count].ChangeNick(nick);
  npc[npc_count].virtual = virtual;
  // ... the rest
  
  npc_count++;
  return (npc_count - 1);
}

//--------------------------------------------------------------------------------


bool Check() {
  int i;
  bool check;
  
  while (i!=lstNames.ItemCount) {
    if (lstNames.Items[i]==ircMessage) check=true;
    i++;
  }
  
  return check;
}

void Act(this Character *, String action) {
  if (action.Length < 1)
    return;

  String msg[] = action.IRCSplit();
  if ((msg[0] == "walks") && (msg[1] == "to")) {
    this.Walk(msg[2].AsInt, msg[3].AsInt, eNoBlock, eWalkableAreas);
  }
  else if ((msg[0] == "screen") && (msg[1] == "to")) {
    object[0].Graphic=msg[2].AsInt;
  }
  else if ((msg[0] == "faces") && (msg[1] == "to")) {
    this.FaceLocation(msg[2].AsInt, msg[3].AsInt, eNoBlock);
  }
   else if ((msg[0] == "view") && (msg[1] == "is")) {
    if (this.View!=msg[2].AsInt) {
      this.ChangeView(msg[2].AsInt);
    }
   }
    else if ((msg[0] == "loop") && (msg[1] == "is")) {
    if ((this.Loop!=msg[2].AsInt) && (!this.Moving)) {
      this.Loop =msg[2].AsInt;
    }
   }
    else if ((msg[0] == "color") && (msg[1] == "is")) {
    if (this.SpeechColor!=msg[2].AsInt) {
      this.SpeechColor=msg[2].AsInt;
    }
   }
  else if ((msg[0] == "is") && (msg[1] == "at")) {
  if (this.ID!=player.ID) {
  this.StopMoving();
    this.x = msg[2].AsInt;
    this.y = msg[3].AsInt;
    this.PlaceOnWalkableArea();
  }
  }
}

//--------------------------------------------------------------------------------

void CafeDo(String action) {
  IRCAction(metachan, action);
  player.Act(action);
}

//--------------------------------------------------------------------------------

void ForceToStage(String nick) {
  Character *body;
  
  if (nick.GetNick() == nickname)
    body = player;
  else {
    int i = NPC.Create(nick, true);
    if (i >= 0) body = npc[i].body;
    else return;
  }
  
  body.StopMoving();
  body.x = 490;
  body.y = 251;
  body.PlaceOnWalkableArea();
  body.Walk(328, 251, eNoBlock, eWalkableAreas);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ForceOnStage(String nick) {
  Character *body;
  
  if (nick.GetNick() == nickname)
    body = player;
  else {
    int i = NPC.Create(nick, true);
    if (i >= 0) body = npc[i].body;
    else return;
  }
  
  body.StopMoving();
  body.x = 334;
  body.y = 240;
  body.PlaceOnWalkableArea();
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ForceOffStage(String nick) {
  Character *body;
  
  if (nick.GetNick() == nickname)
    body = player;
  else {
    int i = NPC.Create(nick, true);
    if (i >= 0) body = npc[i].body;
    else return;
  } 
  
  body.StopMoving();
  body.x = 635;
  body.y = 311;
  body.PlaceOnWalkableArea();
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ReceiveChat(IRCEventType type) {
  if (type == eIRCAction) {
    if ((GetSound(ircMessage, "applau")) || (GetSound(ircMessage, "clap")))   aCr.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("* %s %s", ircFrom, ircMessage));
  }
  else if (type == eIRCMsg) {
    int picture = GetPicture(ircMessage);
    if ((picture > 0) && (lstNames.CheckMode(ircFrom) != "")) object[0].Graphic=picture;
  if ((GetSound(ircMessage, "Congratulations")) && (lstNames.CheckMode(ircFrom) != "")) aAWARDS7.Play(eAudioPriorityHigh, eOnce);
  
  if ((GetSound(ircMessage, "nominee"))  ||(GetSound(ircMessage, "committee")) ||  (GetSound(ircMessage, "nomination"))) {
  if (lstNames.CheckMode(ircFrom) != "")   aAWARDS7.Play(eAudioPriorityHigh, eOnce);
  }
  
  if ((GetSound(ircFrom, "bicilotti")) && (GetSound(ircMessage, "This has been a wonderful night"))&& (lstNames.CheckMode(ircFrom) != "")) aAWARDS9.Play(eAudioPriorityHigh, eOnce);
  if ((GetSound(ircMessage, "applau")) || (GetSound(ircMessage, "clap")))   aCr.Play(eAudioPriorityHigh, eOnce);
  PushLine(String.Format("%s: %s", ircFrom, ircMessage));
    
    int i = NPC.Find(ircFrom);
    if ((i >= 0) && (npc[i].virtual)) {
      if (npc[i].body.y<280)  npc[i].body.SayBackground(ircMessage);
    }
  }
  else if (type == eIRCNotice)
    PushLine(String.Format("*%s*: %s", ircFrom, ircMessage));
  else if (type==eIRCTopic) {
    PushLine(String.Format("TOPIC: %s", ircMessage));
  }
  else if (type == eIRCJoin) {
    aIM_receive.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("(%s has joined)", ircFrom));
    lstNames.AddNick(ircFrom);
    if (ircFrom == nickname)       IRCMode(ircTo, "");
    }
  else if (type == eIRCPart) {
    if (String.IsNullOrEmpty(ircMessage)) ircMessage = "";
    aSend.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("(%s has left: %s)", ircFrom, ircMessage));
    lstNames.RemoveNick(ircFrom);
    
    int i = NPC.Find(ircFrom);
    if ((i >= 0) && (npc[i].virtual)) npc[i].Leave();
  }
  else if (type == eIRCQuit) {
    if (String.IsNullOrEmpty(ircMessage)) ircMessage = "";
    aSend.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("(%s has disappeared: %s)", ircFrom, ircMessage));
    lstNames.RemoveNick(ircFrom);
    
    int i = NPC.Find(ircFrom);
    if ((i >= 0) && (npc[i].virtual)) npc[i].Leave();
  }
  else if (type == eIRCKick) {
    if (ircAction == nickname) {
      Display("They've kicked you out![\"%s\"", ircMessage);
      QuitGame(0);
    }
    aSend.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("(%s has been kicked out by %s: %s)", ircAction, ircFrom, ircMessage));
    lstNames.RemoveNick(ircAction);
    
    int i = NPC.Find(ircAction);
    if ((i >= 0) && (npc[i].virtual)) npc[i].Leave();
  }
  else if (type == eIRCNames) {
    String names[] = ircMessage.IRCSplit();
    lstNames.SetNicks(names);
    
    if (Game.DoOnceOnly("announcers")) {
      int i = lstNames.ItemCount;
      while (i) {
        i--;
        
        String item = lstNames.Items[i];
        
        if (item.GetMode() != "")
          ForceOnStage(item);
      }
    }
  }
  else if (type == eIRCNick) {
    aChat_recieve.Play(eAudioPriorityHigh, eOnce);
    PushLine(String.Format("(%s is now known as %s)", ircFrom, ircAction));
    lstNames.ChangeNick(ircFrom, ircAction);
    
    int i = NPC.Find(ircFrom);
    if ((i >= 0) && (npc[i].virtual)) npc[i].ChangeNick(ircAction);
  }
  else if (type == eIRCMode) {
    aSelect_.Play(eAudioPriorityHigh, eOnce);
    if (ircAction.StartsWith("+") && (ircAction.IndexOf("m") != -1)) {
    	PushLine("*SILENCE* *SILENCE* *SILENCE*");
    	silence = true;
    }
    else if (ircAction == "-m") {
      PushLine("*APPLAUSE* *APPLAUSE* *APPLAUSE*");
      silence = false;
    }
    else if (ircAction == "+o") {
      PushLine(String.Format("(%s presents a new host: %s)", ircFrom, ircMessage));
      lstNames.ChangeMode(ircMessage, "@");
      
      ForceToStage(ircMessage);
    }
    else if (ircAction == "-o") {
      PushLine(String.Format("(%s thanks the host: %s)", ircFrom, ircMessage));
      IRCNames(channel);
      
      ForceOffStage(ircMessage);
    }
    else if (ircAction == "+h") {
      PushLine(String.Format("(%s presents a VIP: %s)", ircFrom, ircMessage));
      lstNames.ChangeMode(ircMessage, "%");
      
      ForceToStage(ircMessage);
    }
    else if (ircAction == "-h") {
      PushLine(String.Format("(%s thanks a VIP: %s)", ircFrom, ircMessage));
      IRCNames(channel);
      
      ForceOffStage(ircMessage);
    }
    else if (ircAction == "+v") {
      PushLine(String.Format("(%s calls %s on stage)", ircFrom, ircMessage));
      lstNames.ChangeMode(ircMessage, "+");
      
      ForceToStage(ircMessage);
    }
    else if (ircAction == "-v") {
      PushLine(String.Format("(%s thanks as %s leaves the stage)", ircFrom, ircMessage));
      IRCNames(channel);
      ForceOffStage(ircMessage);
    }
  }
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void ReceiveMeta(IRCEventType type) {
  if (type == eIRCMsg) {
    int i = NPC.Find(ircFrom);
    if (i >= 0) npc[i].body.SayBackground(ircMessage);
  }
  else if (type == eIRCAction) {
    int i = NPC.Find(ircFrom);
    if (i >= 0) npc[i].body.Act(ircMessage);
  }
  else if (type == eIRCJoin) {
   if (ircFrom != nickname) NPC.Create(ircFrom);

   CafeDo(String.Format("is at %d %d", player.x, player.y));
   CafeDo(String.Format("view is %d %d", player.View, mouse.y));
   CafeDo(String.Format("loop is %d %d", player.Loop, mouse.y));
   CafeDo(String.Format("color is %d %d", player.SpeechColor, mouse.y));
  }
  else if ((type == eIRCPart) || (type == eIRCQuit)) {
    int i = NPC.Find(ircFrom);
    if (i >= 0) npc[i].Leave();
  }
  else if (type == eIRCKick) {
    int i = NPC.Find(ircAction);
    if (i >= 0) npc[i].Leave();
  }
  else if (type == eIRCNick) {
    int i = NPC.Find(ircFrom);
    if (i >= 0) npc[i].ChangeNick(ircAction);
  }
  else if (type == eIRCNames) {
    int i = 0;
    String names[] = ircMessage.IRCSplit();
    while (names[i] != null) {
      if (names[i] != nickname) {
        NPC.Create(names[i]);
        }
      i++;
    }
  }
}

//--------------------------------------------------------------------------------

void Connect(String nick)
{
  if (!IRCConnect(SERVER))  {
    Display("Connection failed!");
    return;
  }
  
  if (String.IsNullOrEmpty(nick))
    nick = "SuperRog";
  
  nickname = nick;
  channel = CHAT_CHANNEL;
  metachan = CAFE_CHANNEL;
  
  IRCNick(nick);
  IRCUser(nick, "agsIRC");


  player.Name = nick;
  
  connected = true;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void Disconnect(){
  connected = false;
  IRCDisconnect();
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

bool IsSilence(){
  return (silence);
}

//--------------------------------------------------------------------------------

function repeatedly_execute() {
  if (!connected)
    return;
  
  IRCEventType type;
  
  type = IRCReceive();
  while (type != eIRCNone) {
    if (type == eIRCWelcome) {
      IRCJoin(channel);
      IRCJoin(metachan, "ninja");
      tick=1;
    }
    else if ((type == eIRCNick) || (type == eIRCQuit)) {
      if ((ircFrom == nickname) && (type == eIRCNick)) {
        nickname = ircAction;
        player.Name = ircAction;
      }      
      ReceiveChat(type);
      ReceiveMeta(type);
    }
    else if (type == eIRCError) {
    Display("Error: %s", ircMessage);
    }
    else if (ircTo == channel)
      ReceiveChat(type);
    else if (ircTo == metachan)
      ReceiveMeta(type);
    
    type = IRCReceive();
  }
}

//--------------------------------------------------------------------------------

void Chat(String msg){
  String cmd;
  
  if (msg.StartsWith("/"))  {
    int pos = msg.IndexOf(" ");
    if (pos >= 2)    {
      cmd = msg.Substring(1, pos - 1);
      msg = msg.Substring(pos + 1, msg.Length - pos - 1);
    }
    else    {
      cmd = msg.Substring(1, msg.Length - 1);
      msg = "";
    }
    
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    
    if (cmd == "me")    {
 if ((lstNames.CheckMode(nickname) == "") && (IsSilence())) {
  Display ("Hush!");
  txtInput.Text="";
  return;
  }
      if ((GetSound(msg, "applau")) || (GetSound(msg, "clap")))   aCr.Play(eAudioPriorityHigh, eOnce);
      IRCAction(channel, msg);
      PushLine(String.Format("* %s %s", nickname, msg));
    }
    else if (cmd == "nick")    {
      IRCNick(msg);
    }
    else if (cmd == "quit")    {
int TempI;
DateTime *dt = DateTime.Now;
String TempS = String.Format("%d/%d/j%d||%d:%d:%d", dt.DayOfMonth, dt.Month, dt.Year, dt.Hour, dt.Minute, dt.Second);
File *theFile = File.Open("log.txt", eFileAppend);
while (TempI < lstbox.ItemCount) {
theFile.WriteRawLine(String.Format("%s     %s",TempS,lstbox.Items[TempI]));
TempI ++;
}
AudioChannel *outro = aAWARDS8.Play(eAudioPriorityHigh, eOnce);
while (outro.IsPlaying) Wait(1);
theFile.Close();
      IRCDisconnect();
      QuitGame(0);
    }
  }
  else  {
  if ((lstNames.CheckMode(nickname) == "") && (IsSilence())) {
  Display ("Hush!");
  txtInput.Text="";
  return;
  }
    IRCMsg(channel, msg);
    IRCMsg(metachan, msg);
   int picture = GetPicture(msg);
	if ((picture > 0) && (lstNames.CheckMode(nickname) != "")) object[0].Graphic=picture;
    if ((GetSound(msg, "Congratulations")) && (lstNames.CheckMode(nickname) != "")) aAWARDS7.Play(eAudioPriorityHigh, eOnce);
  
  if ((GetSound(msg, "nominee"))  ||(GetSound(msg, "committee"))  ||  (GetSound(msg, "nomination"))) {
  if (lstNames.CheckMode(nickname) != "")   aAWARDS7.Play(eAudioPriorityHigh, eOnce);
  }
  
  if ((GetSound(nickname, "bicilotti")) && (GetSound(msg, "This has been a wonderful night"))&& (lstNames.CheckMode(nickname) != "")) aAWARDS9.Play(eAudioPriorityHigh, eOnce);
  if ((GetSound(msg, "applau")) || (GetSound(msg, "clap")))   aCr.Play(eAudioPriorityHigh, eOnce);
     
   PushLine(String.Format("%s: %s", nickname, msg));
    player.SayBackground(msg);
  }
}

//-------------------------------------------------------------------------------- L  // Connection

#define SERVER "irc.bigbluecup.com"
//#define SERVER "localhost"

#define CHAT_CHANNEL "#ags"
#define CAFE_CHANNEL "#cmd2010"

import function GetPicture(String goeshere);
import bool GetSound(String goeshere, String tobecontained);

import void Connect(String nick);
import void Disconnect();

/// Returns true if the channel was muted.
import bool IsSilence();
/// Sends a message or command.
import void Chat(String message);

// Chat window

#define LINE_WIDTH 120

import void PushLine(String line);

import void SetNicks(this ListBox *, String nicks[]);
import void AddNick(this ListBox *, String nick);
import String CheckMode(this ListBox *, String nick);
import String RemoveNick(this ListBox *, String nick);
import void ChangeNick(this ListBox *, String nick, String newnick);
import void ChangeMode(this ListBox *, String nick, String mode);

// Chat cafe

/// Makes the player character do a certain action.
import void CafeDo(String action);

/*
 * Actions:
 *
 * walks to x y
 * is at x y
 *
 * They are defined in the Act function, Chat.asc line 297
 */

#define MAX_NPC 53

struct NPC{
  writeprotected String nick;
  Character *body;
  char mode; // '@' '+' '~' '%' '&' or 0
  bool virtual;
  
  //... add more here if you like
  
/// Changes the nickname of this NPC (nicks may have a mode prefix).
  import void ChangeNick(String nick);
/// Deletes the NPC (when the user quits/parts or is kicked).
  import void Leave();

/// Returns the number of NPCs that currently exists.
  import static int Count(); // $AUTOCOMPLETESTATICONLY$
/// Creates a new NPC and returns its number (nicks may have a mode prefix).
  import static int Create(String nick, bool virtual = false); // $AUTOCOMPLETESTATICONLY$
/// Returns the NPC id by nickname or returns -1 when not found (nicks may have a mode prefix).
  import static int Find(String nick); // $AUTOCOMPLETESTATICONLY$
  
  int id; // Do not use this value
};

import NPC npc[MAX_NPC];

/// Let a character do something
import void Act(this Character *, String action); ���J        ej��