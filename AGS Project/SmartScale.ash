//////////////////////////////////////////////////////////////////////////////////////////////////////////
// SMART SCALE SCRIPT MODULE - Header
//
// Sets the scaling of a character to some multiple of the default character scaling for its position.
// This allows us to mix characters with different sizes and resolutions, and have them scaled so they
// match each other. The scaling factor is read from the avatar.
//
// This method is called in late_repeatedly_execute_always() in GlobalScript.asc, a special version of
// repeatedly_execute_always() that runs after AGS has updated character positions in each game loop.
// Added as an experimental feature by Calin Leafshade
// (see http://www.adventuregamestudio.co.uk/forums/index.php?topic=46347.msg636503206#msg636503206),
// it's provided in a special AGS 3.3 build by CrimsonWizard. If the engine doesn't support
// late_repeatedly_execute_always(), the script is written so it falls back to using
// repeatedly_execute_always(), but in that case there will be flickering when characters walk on
// areas with smooth scaling.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Sets avatar-specific scaling for this character
import void SmartScale(this Character*,  pAvatar avatar, bool doLog = false);