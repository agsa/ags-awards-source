AGSScriptModule        �	  // new module script
Log logger;

void Log::SetLevel(LogLevel level)
{
   this.level = level;
}

String Log::GetLogHistory(int size)
{
   String history = "";
   if(size>this.logHistorySize)
      size = this.logHistorySize;
   int startIndex = (LOG_HISTORY_SIZE + this.logHistoryIndex - this.logHistorySize) % LOG_HISTORY_SIZE;
   int i = this.logHistorySize - size;
   while(i<this.logHistorySize)
   {
      int index = (startIndex + i) % LOG_HISTORY_SIZE;
      history = String.Format("%s[%s",history, this.logHistory[index]);
   i++;
   }
   return history;
}


void Log::Print(String message)
{
   this.logHistory[this.logHistoryIndex] = message;
   this.logHistoryIndex++;
   this.logHistoryIndex = this.logHistoryIndex % LOG_HISTORY_SIZE;
   if(this.logHistorySize < LOG_HISTORY_SIZE)
      this.logHistorySize++;
      
   if(this.logFile != null)
   {
      File* f = File.Open(this.logFile, eFileAppend);
      if(f != null)
      {
         f.WriteRawLine(message);
         f.Close();
      }
   }
   if(this.console != null)
   {
      this.console.Text = this.GetLogHistory();
   }
}

void Log::LogDebug(String message)
{
   if(this.level >= eDebug)
   {
      DateTime* dt = DateTime.Now;
      message = String.Format("DEBUG %02d:%02d:%02d:%04d - %s",dt.Hour, dt.Minute, dt.Second, logger.loopCounter, message);
      this.Print(message);
   }
}

void Log::LogInfo(String message)
{
   if(this.level >= eInfo)
   {
      DateTime* dt = DateTime.Now;
      message = String.Format("DEBUG %02d:%02d:%02d:%04d - %s",dt.Hour, dt.Minute, dt.Second, logger.loopCounter, message);
      this.Print(message);
   }
}

void Log::LogError(String message)
{
   if(this.level >= eError)
   {
      DateTime* dt = DateTime.Now;
      message = String.Format("DEBUG %02d:%02d:%02d:%04d - %s",dt.Hour, dt.Minute, dt.Second, logger.loopCounter, message);
      this.Print(message);
   }
}

void Log::SetConsole(Label* lblOutput)
{
   this.console = lblOutput;
}

void Log::SetLogFile(String fileName)
{
   if(fileName == null)
      this.logFile = null;
   else
   {
      File* f = File.Open(fileName, eFileAppend);
      if(f != null)
      {
         this.logFile = fileName;
         f.Close();
      }
   }
}

void Log::Init(String fileName, Label* lblOutput)
{
   this.SetLogFile(fileName);
   this.SetConsole(lblOutput);
}

function repeatedly_execute_always()
{
   logger.loopCounter++;
}

export logger; �  // new module header
#define LOG_HISTORY_SIZE 6

enum LogLevel
{
   eError, 
   eInfo, 
   eDebug
};

struct Log
{
   String logHistory[LOG_HISTORY_SIZE];
   int logHistoryIndex;
   int logHistorySize;
   int loopCounter;
   
   String logFile;
   Label* console;
   LogLevel level;
   
   import void SetLevel(LogLevel level);
   import void Print(String message);
   import void LogDebug(String message);
   import void LogInfo(String message);
   import void LogError(String message);
   import void SetConsole(Label* lblOutput);
   import void SetLogFile(String fileName);
   import void Init(String fileName, Label* lblOutput);
   import String GetLogHistory(int size=LOG_HISTORY_SIZE);
};

import Log logger; �%#        ej��