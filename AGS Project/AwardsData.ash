//////////////////////////////////////////////////////////////////////////////////////////////////////////
// AWARDS DATA SCRIPT MODULE - Header
//
// This module populates the data for the current awards. Ideally, to update the ceremony for another
// year, the only thing you should have to do is update this script module (along with sprites and other
// resources for the nominations), specifically the initNominations() function.
//
// Awards data consists of categories[] - an array of AwardsCategory instances (which mainly consist of
// some text to display and the nominees for that category), and nominees[] - an array of all the
// AwardsNominee instances. The AwardsNominee defines all the relevant data for a game/character/etc.
// that has been nominated: its name, as well as graphics to display and sound to play for each category
// it appears in. All the graphics for one nominee are stored as a view, with one animation loop per
// category (and the very first used when it's declared winner).
//
// The logic for displaying it all correctly is handled in DirectorCommands.asc.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////


#define CATEGORY_COUNT 19  // How many categories we have.
#define NOMINEE_COUNT  44  // Includes juried awards (lifetime achievement, best innovation), and characters separately from their games
// ... + special aftershow performances

#define CATEGORY_SLOTS 20 // MUST BE CATEGORY_COUNT + 1!
#define NOMINEE_TRIGGER_MAX 4
#define CATEGORY_NOMINEES_MAX 13

/// All the different Awards Categories
enum AwardsCat
{
  eBestGame,             // 1
  eBestFreewareGame,
  eBestGameplay,
  eBestPuzzles,
  eBestWriting,          // 5
  eBestCharacter,
  eBestShortGame,        // 7
  eBestNonAdventure,
  eBestDemo,
  eBestBackgroundArt,    // 10
  eBestAnimation,
  eBestCharacterArt,     // 12
  eBestProgramming,
  eBestMusicSound,
  eBestVoiceWork,        // 15
  eMaggies,
  eLifetimeAchievement,
  eBestInnovation,       // 18
  eAfterShow             // 19
};

/// Pseudo-pointer to an AwardsCategory (used as index into the categories[] array)
enum AwardsCategoryPtr {};
/// Pseudo-pointer to an AwardsNominee (used as index into the nominees[] array)
enum AwardsNomineePtr {};

/// Definees a nominee
struct AwardsNominee
{
  /// Name of this nominee
  String title;
  /// String that can be used as a shortcut instead of typing full title
  String shortcut;
  /// Array of "trigger" strings. If an announcer says something that contains any of these strings, it will be taken as announcing that nominee
  String trigger[NOMINEE_TRIGGER_MAX];

  /// View to display on screen (on top)
  int vScreenFront;
  /// Sound to play when presenting nomination for different categories (if null, just the regular fanfare)
  AudioClip* sound[CATEGORY_SLOTS];
  /// Music to play when giving victory speech (if null, standard acceptance music)
  AudioClip* music;
  /// How quickly to display the animation (negative numbers for slideshow transition)
  int animationSpeedFront[CATEGORY_SLOTS];
  /// Whether this nominee is secret (shouldn't be listed in the program); mostly for juried awards
  bool secret;
};

/// Defines an awards category
struct AwardsCategory
{
  /// Official title of the category (e.g. "Best Game Created with AGS")
  String fullTitle;
  /// Short title of the category (e.g. "Best Game")
  String shortTitle;
  /// Type of this category (e.g. "Technical Award", "Main Award", etc.)
  String categoryType;
  /// The enum value of this award (slightly redundant)
  AwardsCat cat;
  /// Whether this awards category is secret; mainly for "special" awards and pseudo-categories like the AfterShow
  bool secret;

  /// The nominees for this award (as pointers into nominees[])
  AwardsNomineePtr nominees[CATEGORY_NOMINEES_MAX];
  /// How many nominees there are for this award
  int nomineeCount;
  /// Add a candidate as nominee for this award
  import void Nominate(AwardsNomineePtr nominee);
  /// Helper method to get the number of nominees to the award with or without those that are secret
  import int GetNomineeCount(bool includeSecret);
};

// Additional API for dealing with Awards Data
struct Awards
{
  /// Finds the index of a category enum value in the categories[] array (not currently used)
  import static AwardsCategoryPtr GetCategory(AwardsCat cat);
};

/// The awards nominees
import AwardsNominee nominees[NOMINEE_COUNT];
/// The awards categories
import AwardsCategory categories[CATEGORY_COUNT];

// Index values for special (pseudo-)nominees
import int nomineeModsSong;
import int nomineeTrollSong;
import int nomineeDimLights;
//import int nomineeCavalcade2016;
//import int nomineeNominees2016;