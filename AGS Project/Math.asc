//////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATH FUNCTIONS SCRIPT MODULE - Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////////

static int Math::Abs(int value)
{
  if(value < 0) return -value;
  else return value;
}

static int Math::Max(int v1, int v2)
{
  if(v1>v2) return v1;
  else return v2;
}

static int Math::Min(int v1, int v2)
{
  if(v1<v2) return v1;
  else return v2;
}

static int Math::Clamp(int min, int max, int value)
{
  if(value<min) return min;
  if(value>max) return max;
  else return value;
}

static bool Math::GetBit(int field, int bit)
{
  return (field & (1 << bit) != 0);
}

static int Math::SetBit(int field, int bit, bool value)
{
  if(value)
    return (field | (1 << bit));
  else
    return (field ^ (field & (1 << bit)));
}