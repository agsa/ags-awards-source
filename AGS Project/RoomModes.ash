//////////////////////////////////////////////////////////////////////////////////////////////////////////
// ROOM MODES SCRIPT MODULE - Header
//
// This module allows the room to be set to different states (currently just different lighting
// conditions), and to switch or fade between them. This is achieved by direct-drawing different versions
// of the room (with varying opacity) on the Room.GetDrawingSurfaceForBackground() surface.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define OCURTAINS 3
#define OLECTERN 4
#define OTROPHY 5

#define RAFTERS_Y 150
#define TROPHY_Y 461
#define LECTERN_Y 468
#define TRAPDOOR_DROP_Y 570

#define TROPHY_AGS 5989
#define TROPHY_MAGGIES_GOLD 5918
#define TROPHY_MAGGIES_SILVER 5919
#define TROPHY_MAGGIES_BRONZE 5917

/// The different room modes possible
enum LightMode
{
  eRoomNormal = 0,    // Normal lighting (bright)
  eRoomDarkened,      // Room and stage darkened
  eRoomStageLight,    // Room darkened, stage bright
  MAX_LIGHT_MODE
};

/// API to control the room mode
struct RoomMode
{
  import static LightMode GetCurrentLightMode();
  /// Switch to a certain room mode immediately
  import static void SetLights(LightMode toLightMode);
  /// Fade to a certain room mode
  import static void TweenLights(LightMode toLightMode, float time);
};