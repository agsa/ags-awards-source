//////////////////////////////////////////////////////////////////////////////////////////////////////////
// TREE DATA STRUCTURE SCRIPT MODULE - Header
//
// Represents a tree data structure, with unlimited children per node. The implementation is general,
// but primarily intended for tree-like branching menus (such as the director console). The tree is
// stored as an array of Tree nodes. The root is usually in Tree_nodes[0].
//
// Each node stores information to allow us to traverse the tree, whether it is expanded or collapsed,
// and a flag as to whether more than one of its children can be expanded at a time. There's also an
// array, data[], which can store arbitrary information per node.
//
// Written by Snarky, 2016.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define TREE_MAX_SIZE 256

enum pTree {};

struct Tree
{
  writeprotected pTree id;
  writeprotected pTree parentId;
  writeprotected pTree firstChildId;
  writeprotected pTree nextSiblingId;
  writeprotected int childCount;
  bool isExpanded;
  bool allowMultiExpand;

  String text;
  int data[5];   // Allows us to store various data

  import bool IsValid();

  import static void Init();
  import static pTree AddRoot();
  import static pTree GetRoot();
  import static pTree AddNode(pTree parentId=0,  int siblingPos=-1);
  import static bool RemoveNode(pTree id);

  import static void CollapseSubTree(pTree subRoot);
  import static pTree TraverseToIndex(int index);
  import static String Print();
  import static int Size();
};

import void SetExpanded(this Tree*,  bool value);

/// The tree nodes
import Tree Tree_nodes[TREE_MAX_SIZE];