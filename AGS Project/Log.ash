//////////////////////////////////////////////////////////////////////////////////////////////////////////
// LOGGING SCRIPT MODULE - Header
//
// An easy API for logging program events. Call it with, e.g.:
//
//    logger.logDebug("myFunction called");
//
// To include variable values etc. in the log entry, use String.Format():
//
//    logger.logDebug(String.Format("myFunction called with argument=%d", myVariable));
//
// The log can be written to a file and displayed in-game on a GUI Label. To initialize, use:
//
//    logger.Init("mylog.log",lblLogDisplay); // lblLogDisplay is the name of your GUI label (can be null)
//
// This is how entries appear in the log. The last part of the timestamp is the game loop number:
//
//    DEBUG 18:14:57:0276 - myFunction called with argument=5
//
// You can also set a log level, to only include events above a certain priority. (For example to not
// include debug entries in the release version of the game.):
//
//    logger.SetLevel(eError); // Only entries with error level will be included in the log
//
// One log ("logger") is provided by default, but you can create more if you like, for instance in order
// to log different things to different files.
//
// Because AGS Script is not the most efficient, heavy use of logging could slow down the the game.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// How many of the last few log entries to store in-game (e.g. to display on a GUI Label)
#define LOG_HISTORY_SIZE 6

/// What priority level of log entries to include in the log
enum LogLevel
{
  eError,
  eInfo,
  eDebug
};

/// A log for recording application events
struct Log
{
  /// A cache of the last few log entries (a circular buffer)
  String logHistory[LOG_HISTORY_SIZE];
  /// The index we're currently at in the logHistory (where to write the next entry)
  int logHistoryIndex;
  /// How many log entries we have cached
  int logHistorySize;

  /// The filename of our log file
  String logFile;
  /// The GUI label we display the log on (can be null)
  Label* console;
  /// The current logging level (the lowest priority of log entries that will be written)
  LogLevel level;

  /// Set the logging level
  import void SetLevel(LogLevel level);
  /// Write a string to the log file and GUI console (used by the other logging methods)
  import void Print(String message);
  /// Write a log entry with level eDebug
  import void LogDebug(String message);
  /// Write a log entry with level eInfo
  import void LogInfo(String message);
  /// Write a log entry with level eError
  import void LogError(String message);
  /// Set the GUI console (the GUI label we display the log on). Can be null
  import void SetConsole(Label* lblOutput);
  /// Set the log file. Can be null
  import void SetLogFile(String fileName);
  /// Initialize the log. Each argument can be null (but if both are, it's pointless)
  import void Init(String fileName, Label* lblOutput);
  /// Get the last few log entries as a String
  import String GetLogHistory(int size=LOG_HISTORY_SIZE);

  // Get the loop count: how many game loops the game has been running for
  import static int GetLoopCount();
};

/// The default log
import Log logger;