//////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATH FUNCTIONS SCRIPT MODULE - Header
//
// Some basic mathematical functions.
//
// Written by Snarky.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

struct Math
{
  /// Absolute value of an integer
  import static int Abs(int value);
  /// Maximum of two integers
  import static int Max(int v1,  int v2);
  /// Minimum of two integers
  import static int Min(int v1,  int v2);
  /// Clamp value to range
  import static int Clamp(int min, int max, int value);

  /// Treating an integer as a bit field (string of binary values), read the nth smallest bit
  import static bool GetBit(int field, int bit);
  /// Treating an integer as a bit field (string of binary values), set the nth smallest bit
  import static int SetBit(int field, int bit, bool value);
};
