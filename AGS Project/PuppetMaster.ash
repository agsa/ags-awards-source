//////////////////////////////////////////////////////////////////////////////////////////////////////////
// PUPPETMASTER SCRIPT MODULE - Header
//
// This module allows the game to take control over certain characters, forcing them to move to
// particular locations and only returning control to the user once they've arrived. This is used
// in particular for "virtual" characters (those representing IRC-only participants who don't control
// their characters), and for when people get on and off stage. Lots more to do here.
//
// Written by Dualnames and Snarky.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

import void ForceOnStage(String nick,  bool instant = false);
import void ForceOffStage(String nick);
import void ForceAllToWings();
import void EnterHall(this Character*);