//////////////////////////////////////////////////////////////////////////////////////////////////////////
// DIRECTOR CONSOLE SCRIPT MODULE - Header
//
// This module handles logic for the director console, the dynamic listbox by which the director can
// control the ceremony. A simplified version (without control) is rendered for other participants.
// All actions simply call functions in the DirectorCommands script.
// Part of the AGS Awards ceremony game (not a standalone module).
//
// Written by Snarky based (very loosely) on code by Dualnames.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

struct DirectorConsole
{
  /// Re-render the Program/Director console
  import static void Refresh();

  /// Processes a command from the Program/Director console
  import static void RunCommand();
  /// Prints the current displayed menu and the underlying tree structure to the log
  import static void PrintToLog();
};