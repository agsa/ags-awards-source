//////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIMING QUEUES SCRIPT MODULE - Header
//
// This module deals with events that are delayed: the action happens some time after the input.
// This is done in order to collect multiple inputs into one event (cf. a double-click), to reduce the
// number of events (throttling the rate at which you can press a button, for example), or to avoid a
// "flood" of events all happening at the same time.
//
// One of the main parts of this module is the Applause logic, which tries to play an appropriate audio
// clip depending on how many people are clapping (have pressed the clap button recently).
// This isn't working 100% yet.
//
// The ClapButton is for throttling the clap button and to support different "intensities" of applause.
// The player can double- or triple-click it to increase the intensity of their clapping, but after that
// it's disabled for a short period, so that they can't spam the channel with clap messages. This seems
// to work pretty well.
//
// The AnnounceHeap keeps track of people who have logged on but haven't been sent the player's location,
// avatar information, etc., so they don't know how or where to display our character. We delay sending
// this info for a random length of time after they log on in order to not flood IRC with messages when
// there are many participants online. We also send the as private messages instead of broadcasting to the
// CAFE_CHANNEL in order to reduce traffic, unless there are more than a certain number of people waiting
// for our update, in which case we just broadcast it. This replaces an earlier IRC bot that sent all of
// this information to people logging on immediately, which kept crashing. It seems to be working fine.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// API for throttling the clap button
struct ClapButton
{
  /// Indicate that the clap button was pressed
  import static void Clap();
  /// Check whether the button is currently enabled
  import static bool Enabled();
  /// Check how many times the button was pressed (single-click, double-click or triple-click)
  import static int GetClapLevel();
  /// Check whether the button has finished registering an event (single-click, double-click or triple-click)
  import static bool ReadyToSend();
  /// Encode the click as a string ("clap" repeated as many times as the button was pressed)
  import static String ToString();
  /// Let the button know that the click was registered, and it should now disable itself for a while
  import static void Consume();
};

/// API for controlling applause. Internal logic is... messy
struct Applause
{
  /// Adds a clap to the applause. Claps can come in different degrees of intensity
  import static void Add(int intensity);
  /// Finds the current overall applause intensity
  import static int GetIntensity();
  /// Find the current overall applause volume
  import static int GetVolume();
  /// Checks to see whether this String (from a chat message/action) represents a clap (unused)
//   static int Match(String s);
};

// A "heap" (actually a sorted list) of people you need to announce your position/look to
struct AnnounceHeap
{
  /// Check how many remote participants are waiting for our announcement
  import static int GetSize();
  /// Add a participant that we should announce to in delayLoops game loops
  import static int Add(int delayLoops, String announceTo);
  /// Clear the list of participants waiting for announcement (for when we broadcast our announcement to everyone)
  import static void Clear();
  /// Check whether it's time to announce to anyone
  import static bool IsDeadline();
  /// Get the first person in line for our announcement
  import static String GetRecipient();
  /// Indicate that we have announced to the first person in line, so remove from list
  import static void Consume();
};