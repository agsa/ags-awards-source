/******************************************************************************
 *                                                                            *
 *  AGS AWARDS CEREMONY 2014: AGS-IRC CLIENT                                  *
 *                                                                            *
 *  Original AGS-IRC client by Wyz                                            *
 *  AGS Awards Ceremony by Dualnames with help by Wyz                         *
 *  2014 Awards client updated by Snarky                                      *
 *                                                                            *
 *  Graphics and sound assets by various people.                              *
 *                                                                            *
 ******************************************************************************/

// Main header script

#define MUSIC_GRAPHIC_S 6267
#define MUSIC_MUTED_GRAPHIC_S 6266
#define MUSIC_GRAPHIC_G 6373
#define MUSIC_MUTED_GRAPHIC_G 6374

#define SOUND_GRAPHIC_S 6273
#define SOUND_MUTED_GRAPHIC_S 6271
#define SOUND_GRAPHIC_G 6378
#define SOUND_MUTED_GRAPHIC_G 6379


