//////////////////////////////////////////////////////////////////////////////////////////////////////////
// DIRECTOR COMMANDS SCRIPT MODULE - Script
//
// This module handles special commands for the "director" of the event, to control the screen,
// music, flow of the event etc. Part of the AGS Awards ceremony game (not a standalone module).
//
// Written by Snarky based on code by Dualnames.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MUSIC_TRACKS_SIZE 35
#define SCREEN_IMAGES_SIZE 9 // How many images we can display in tiled mode

/// Broadcasts director messages over IRC (encoded into the topic of the CAFE_CHANNEL, values set with AwardsProgramState)
import bool SendDirectorData();

/// Parses and executes director messages received over IRC
import bool ReadDirectorData(String msg);

enum MusicGroup
{
  eMusicJukebox,
  eMusicBackground,
  eMusicGameOld,
  eMusicGame
};

enum pMusicEntry {};
struct MusicEntry
{
  String title;
  String artist;
  MusicGroup group;

  AudioClip* track;
  int id;
};

enum pScreenImage {};
struct ScreenImage
{
  String name;
  int image;        // Graphic or view number
  bool isAnimated;
  int animationSpeed;
};

struct Director
{
  import static void SetMusic(pMusicEntry track, bool broadcast=true);
  import static void SetScreen(pScreenImage screen, bool broadcast=true);
  import static void SetLights(LightMode mode, bool broadcast=true);
  import static void SetLectern(bool on, bool broadcast=true);
  import static void SetTrophy(Trophy trophy, bool broadcast=true);

  import static void SetCategory(AwardsCategoryPtr cat, bool broadcast=true);
  import static void ResetCategory(AwardsCategoryPtr cat, bool broadcast=true);
  
  import static void PlayIntro(AwardsCategoryPtr cat);
  import static void ShowNominee(int nomIndex, bool broadcast=true);
  import static void ShowAnnounceDrumroll(AwardsCategoryPtr cat, bool broadcast=true);
  import static void SetWinner(AwardsCategoryPtr cat, int nomIndex, bool broadcast=true);

  import static void Laugh(bool broadcast=true);

  import static void AdmireTrophy(Trophy trophy);
  import static void StartGrooving(pMusicEntry track, bool broadcast=true);
  import static void StopGrooving(bool broadcast=true);
  import static void StartDrumroll(bool broadcast=true);
  import static void StopDrumroll(bool broadcast=true);
  import static void StartConfetti(bool broadcast=true);
  import static void StopConfetti(bool broadcast=true);
};

import MusicEntry musicTracks[MUSIC_TRACKS_SIZE];
import ScreenImage screenImages[SCREEN_IMAGES_SIZE];