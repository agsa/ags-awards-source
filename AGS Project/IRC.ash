/******************************************************************************
 * IRC module for AGS                                                         *
 *    v alpha1                                                                *
 *                                                                            *
 * Author: Ferry "Wyz" Timmers                                                *
 *                                                                            *
 * Date: November 2010                                                        *
 *                                                                            *
 * Dependencies: agssock.dll (v alpha 1)                                      *
 *                                                                            *
 * Description: A IRC (internet relayed chat) client.                         *
 *                                                                            *
 ******************************************************************************


/*----------------------------------------------------------------------------*\
                              Connection
\*----------------------------------------------------------------------------*/

/// Opens a connection to a irc server, returns true on success
import bool IRCConnect(String address, int port = 6667);
/// Closes the opened irc connection
import void IRCDisconnect();

/// Sends a raw irc command. (You usually don't need this)
import void IRCRaw(String command);

/// Returns the next IRCCommand from the input buffer or null if empty. (You usually don't need this)
import String IRCReceiveRaw();
// See below (irc events) for IRCReceive();

/*----------------------------------------------------------------------------*\
                              Registration
\*----------------------------------------------------------------------------*/

/// Sets or changes the nick name.
import void IRCNick(String nick);
/// Sends user information to irc server as part of de identification.
import void IRCUser(String email, String realname);
/// Whether we are connected to IRC
import bool IRCIsConnected();

/*----------------------------------------------------------------------------*\
                              Channel operations
\*----------------------------------------------------------------------------*/

/// Joins an irc channel (use key when channel is protected by a password)
import void IRCJoin(String channel, String key = 0);
/// Parts an irc channel
import void IRCPart(String channel, String message = 0);

/// Sets a user or channel mode.
import void IRCMode(String to, String flags, String parameter = 0);
/// Checks, sets or clears a channel topic.
import void IRCTopic(String channel, String topic = 0);
/// Lists the users that are on a certain channel.
import void IRCNames(String channel);
/// Lists the channel information of a certain channel.
import void IRCList(String channel);

/// Invites a user to a channel.
import void IRCInvite(String channel, String nick);
/// Kicks a user from a channel.
import void IRCKick(String channel, String nick, String message = 0);

/*----------------------------------------------------------------------------*\
                              Sending messages
\*----------------------------------------------------------------------------*/

/// Sends a chat message to either a channel or an user privately.
import void IRCMsg(String to, String message);
/// Sends a notice to either a channel or a user privately.
import void IRCNotice(String to, String message);
/// Sends an action to either a channel or a user privately.
import void IRCAction(String to, String message);

// Sends a custom client-to-client-protocol message to either a channel or a user privately.
import void IRCCtcp(String to, String name, String message = 0);
// Sends a custom client-to-client-protocol reply to either a channel or a user privately.
import void IRCCtcpReply(String to, String name, String message = 0);

/*----------------------------------------------------------------------------*\
                              IRC events (receiving)
\*----------------------------------------------------------------------------*/

// Event variables
// *NOTE*: These are globally defined so fetch them directly after calling IRCReceive();

import String ircFrom; // Nick name of the user that caused the event to happend
import String ircTo;   // Channel / user where the event was targetted (if any)

// See eventtype definition for information about these:
import String ircAction;
import String ircMessage;

enum IRCEventType
{
  eIRCNone,
  eIRCNext,

  /* User / channel events */
  eIRCNick,   // message = new name
  eIRCQuit,   // message = quit message from user

  eIRCJoin,
  eIRCPart,   // message = part message from user
  eIRCMode,   // action = flags, message = parameter
  eIRCTopic,  // message = topic
  eIRCInvite,
  eIRCKick,   // action = nick of kicked user, message = kick message from user

  eIRCMsg,    // message = chat message
  eIRCNotice, // message = notification
  eIRCAction, // message = action description
  eIRCCtcp,   // action = ctcp command, message = ctcp arguments
  eIRCCtcpReply,
  // name = ctcp command, message = ctcp arguments
  // *NOTE*: Do not send any message in return on ctcp replies, this will create a loop!

  /* Server replies */
  eIRCWelcome,// message = welcome message
  eIRCList,   // action = amount of visble users, message = topic
  eIRCListEnd,
  eIRCNames,  // message = list of nick names seperated by spaces (use message.IRCSplit())
  eIRCError,  // action = error code (see RFC 2812), message = readable error message
};

/// Returns the next Event from the input buffer. (also handles some events automatically)
import IRCEventType IRCReceive();
/// Returns the next Event from the specified string. (You usually don't need this)
import IRCEventType IRCProcess(String command);

/// Returns a IRC tokenized list as a dynamic String array. (last element <=> null)
import String []IRCSplit(this String *);

/*----------------------------------------------------------------------------*\
                              IRC message processing structs
                              (You usually don't need this)
\*----------------------------------------------------------------------------*/

#define IRC_MAX_ARGS 64

struct IRCCommand
{
  String sign;
  String name;
  String args[IRC_MAX_ARGS];
  String msg;

/// Returns the contents of the struct as a string. (See RFC 1459)
  import String Pack();
/// Sets the contents of the struct by string. (See RFC 1459)
  import void Unpack(String packed);
};

struct IRCUsersign
{
  String nick;
  String user;
  String host;

/// Returns the contents of the struct as a string. (See RFC 1459)
  import String Pack();
/// Sets the contents of the struct by string. (See RFC 1459)
  import void Unpack(String packed);
};