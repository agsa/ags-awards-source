//////////////////////////////////////////////////////////////////////////////////////////////////////////
// NPC SCRIPT MODULE - Header
//
// This module holds data for "NPCs": characters controlled remotely by other participants over IRC.
// We need to keep track of their IRC information (username etc.), the character they're controlling in
// the game, and which avatar they're using, among other things.
//
// For convenience, we also have an "NPC" instance for the local player.
//
// Written by Dualnames, with some edits by Snarky.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MAX_NPC 45   // was 90
#define NPC_CHAR_START 1
#define NPC_CHAR_END 60 // was 70 // Non-inclusive

struct NPC
{
  /// The IRC nickname of this NPC
  writeprotected String nick;
  /// The AGS character controlled by this NPC
  Character* body;
  /// The current avatar this NPC is using
  pAvatar avatar;
  char mode; // '@' '+' '~' '%' '&' or 0
  /// Whether this NPC is only participating over plain IRC rather than the AGS client (i.e. not controlling a character)
  bool virtual;
  /// Whether this NPC is currently moving under the command of game logic rather than user control (cf. PuppetMaster.asc)
  bool puppeteered;
  //... add more here if you like

  /// Changes the nickname of this NPC (nicks may have a mode prefix).
  import void ChangeNick(String nick);
  /// Deletes the NPC (when the user quits/parts or is kicked).
  import void Leave();
  /// Carry out an instruction received remotely
//   import void Act(String action);
  /// Update this NPC's avatar
  import void ChangeAvatar(pAvatar avatar);
  /// Outputs this character as a string
  import String ToString();

  /// Returns the number of NPCs that currently exists.
  import static int Count(); // $AUTOCOMPLETESTATICONLY$
  /// Creates a new NPC and returns its number (nicks may have a mode prefix).
  import static int Create(String nick, bool virtual = false); // $AUTOCOMPLETESTATICONLY$
  /// Returns the NPC id by nickname or returns -1 when not found (nicks may have a mode prefix).
  import static int Find(String nick); // $AUTOCOMPLETESTATICONLY$
  int id; // The index of this NPC in npcs[]. Do not use this value (used internally)
  /// Writes a list of all NPCs and their fields to the log
  import static void LogNPCs();
};

/// Other participants. Characters controlled remotely over IRC
import NPC npc[MAX_NPC];

/// The player
import NPC pc;