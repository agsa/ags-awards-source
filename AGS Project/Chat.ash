//////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHAT CAFE SCRIPT MODULE - Header
//
// Handles a lot of the logic to get multiplayer interaction in the app.
// Part of the AGS Awards ceremony game (not a standalone module).
//
// Written mostly by Dualnames (and Wyz?), with some edits by Snarky.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//import function GetPicture(String goeshere);
//import bool GetSound(String goeshere, String tobecontained);

/// Attempts to connect to IRC with chosen nick (server and channels set as GlobalVars)
import void Connect(String nick, String nickPassword);
/// Disconnects from IRC
import void Disconnect();

/// Returns true if the channel was muted.
import bool IsSilence();
/// Sends a message or command.
import void Chat(String message);

// Chat window

#define LINE_WIDTH 105

import void PushLine(String line);

// Chat cafe

/// Makes the player character do a certain action.
import void CafeDo(String action);

import String EncodePos(this Character*, pAvatar avatar);
import String EncodeWalkTo(this Character*, pAvatar avatar, int destX, int destY);

import void SetNicks(this ListBox *, String nicks[]);
import void AddNick(this ListBox *, String nick);
import String CheckMode(this ListBox *, String nick);
import String RemoveNick(this ListBox *, String nick);
import void ChangeNick(this ListBox *, String nick, String newnick);
import void ChangeMode(this ListBox *, String nick, String mode);

/*
 * Actions:
 *
 * walks to x y
 * is at x y
 *
 * They are defined in Chat.asc Act()
 */


/// Let a character do something
//import void Act(this Character *, String action);

import int destinationX;
import int destinationY;
