////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

#define CALL_PROFILE_CREATED 1
#define CALL_NEW_PROFILE 2
#define CALL_LOAD_PROFILE 3

#define CHAT_WIDTH 1280
#define CHAT_HEIGHT_COLLAPSED 75
#define CHAT_HEIGHT_EXPANDED 200
#define CHAT_BOTTOM 720
#define CHAT_INSET_LEFT 16
#define CHAT_INSET_RIGHT 24
#define CHAT_INSET_TOP 8
#define CHAT_INSET_BOTTOM 52

import void PickColor(GUIControl* control);
import bool SetNick(String nick, String nickPassword);

import void ShowAvatarsMenu(bool fullscreen);
import void ShowTextColorMenu(bool fullscreen);
import void ShowNickMenu(bool fullscreen);

/// Send a line on the chat input. Returns whether something was sent (for throttling purposes)
import bool SendText(String text);

