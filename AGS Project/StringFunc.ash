//////////////////////////////////////////////////////////////////////////////////////////////////////////
// STRING FUNCTIONS SCRIPT MODULE - Header
//
// Various operations that can be performed on Strings. Some of them very general, others specifically
// for parsing IRC usernames.
//
// Written by Snarky and Dualnames (and Wyz?).
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Checks whether a string is a valid integer (before parsing with String.AsInt)
import bool IsInteger(this String*);
/// Splits a string into sections separated by the divider, and returns an array of the sections. (The length of the array is in the first slot)
import String[] Split(this String*, String divider);

/// Extracts the nickname from an IRCFrom string (I think mainly to strip away modifier characters like ~, @, +, etc.)
import String GetNick(this String*);
/// Extracts the mode from an IRCFrom string (the modifier character, e.g. ~, @, +...)
import String GetMode(this String*);
/// Presumably performs an alphabetical sort of the strings in the array
import void SortArray(String array[]);
/// Performs AGS-encoding of a string (encoding control character, result is String as AGS needs it to display correctly in e.g. Labels)
import String AgsEncode(this String*);
/// Performs AGS-decoding of a string (decoding control characters, result is String as it should appear)
import String AgsDecode(this String*);