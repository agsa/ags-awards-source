//////////////////////////////////////////////////////////////////////////////////////////////////////////
// AVATARS SCRIPT MODULE - Header
//
// Holds data about the available avatars and handles avatar selection, including GUI display.
// Part of the AGS Awards ceremony game (not a standalone module). Depends on gAvatars* GUIs.
//
// Written by Snarky based on code by Dualnames (and Wyz?).
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// How many avatars there are to choose between
#define AVATAR_COUNT    138

// Text input field for entering avatar group
import TextField* txtAvatarPopupInput;

/// Represents relevant avatar information
struct Avatar
{
  /// View index to use as walkview
  int WalkView;
  /// View index to use as talkview (currently unused)
  int TalkView;
  /// View index to use for special animation (currently unused)
  int SpecialView;
  /// Name of this character - NOTE: If two avatars have exactly the same name, it could lead to trouble with Avatars.Find()
  String Name;
  /// Which game this avatar comes from
  String Source;
  /// Whether this character is male or female (currently unused, could be used to filter avatars)
  bool IsMale;
  /// Which group of avatars this belongs to (also used as password)
  String Group;
  /// How to scale this avatar when drawing (in percent, 100 = normal scaling)
  int ScalingFactor;
};

/// An int that is being used as pointer to an avatar (index into Avatars_Avatars)
enum pAvatar
{
};

/// API for the Avatars module
struct Avatars
{
  /// Sets the avatar selection GUIs to display avatars from the selected group
  import static void SetupAvatarsDisplay(String group, int screen=0); // $AUTOCOMPLETESTATICONLY$
  /// Gets the current player avatar
  import static pAvatar GetPlayerAvatar(); // $AUTOCOMPLETESTATICONLY$
  /// Sets the current player avatar
  import static void SetPlayerAvatar(pAvatar avatar); // $AUTOCOMPLETESTATICONLY$
  /// Shows the avatar selection GUIs, optionally displaying OK button (default off)
  import static void ShowGui(String startGroup, bool showOk); // $AUTOCOMPLETESTATICONLY$
  /// Hides the avatar selection GUIs
  import static void HideGui(); // $AUTOCOMPLETESTATICONLY$
  /// Handles button clicks on any of the avatar selection GUIs
  import static pAvatar OnButtonClick(GUIControl* control, MouseButton button); // $AUTOCOMPLETESTATICONLY$
//   import static void WriteProfile(); // $AUTOCOMPLETESTATICONLY$
  /// Reads the player avatar from the profile (which should be loaded beforehand)
  import static void ReadProfile(); // $AUTOCOMPLETESTATICONLY$
  /// Finds the index of the first character of that name (-1 if not found)
  import static pAvatar Find(String name); // $AUTOCOMPLETESTATICONLY$
  /// Finds the nth avatar in the selected group (wrapped around)
  import static pAvatar Pick(String group,  int n);
  /// Finds the nth avatar in the selected group (wrapped around)
  import static pAvatar FindPersonal(String nick);
};

/// Setting special avatars for IRC-only people
struct AvatarLookup
{
  String name;
  pAvatar avatar;
};

/// Sets a character to use a particular avatar (its walkview and scaling)
import void SetAvatar(this Character*, pAvatar avatar);

/// Master avatar lookup table
import Avatar Avatars_Avatars[AVATAR_COUNT];