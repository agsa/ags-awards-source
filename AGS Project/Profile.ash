//////////////////////////////////////////////////////////////////////////////////////////////////////////
// PROPERTIES PROFILE SCRIPT MODULE - Header
//
// Allows you to store and retrieve "properties" (key-value pairs like "username=dummy"). These can be
// written to and read from a file. This allows for configuration files, or for saving data outside
// of saved games. The implementation is pretty simple, so don't do anything stupid like using an "="
// sign in a property name (should be OK in a value, though).
//
// The maximum number of properties supported is defined as the MAX_PROPERTIES constant in the script,
// and is 10 by default. Change it if you need more.
//
// Written by Snarky, 2015.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// API to store and retrieve properties
struct Profile
{
  /// Set the value of a property. Returns true if successful
  import static bool SetProperty(String key,  String value);
  /// Get the value of a property, or null if not found
  import static String GetProperty(String key);
  /// Import properties stored in a file. Will overwrite current values of existing properties by the same name
  import static bool ReadFromFile(String fileName);
  /// Export the current properties to a file. Will completely overwrite any existing content in that file!
  import static bool WriteToFile(String fileName);
};

// Defined properties
#define PROP_IRC_SERVER = "ircserver"
#define PROP_CHAT_CHANNEL = "chatchannel"
#define PROP_CHAT_PW = "chatpassword"
#define PROP_CAFE_CHANNEL = "cafechannel"
#define PROP_CAFE_PW = "cafepassword"
#define PROP_NICKNAME "nick"
#define PROP_NICK_PW = "nickpassword"

#define PROP_TEXT_COLOR "textcolor"
#define PROP_AVATAR "avatar"
#define PROP_AVATAR_PW "avatarpassword"

// (Volume properties are autogenerated)

#define PROP_DIRECTOR_PW = "directorpassword"
#define PROP_HOST_PW = "hostpassword"