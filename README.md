# AGS Awards Open Source

Open-source version of the AGS Awards Ceremony client.

This is a multiplayer game client made in AGS, for an annual awards ceremony live event.

# License
The code is licensed under the Artistic License 2.0.
The assets (graphics, music, etc.) are under various licenses.

Note that many assets from nominated games are not licensed for use outside the ceremony.