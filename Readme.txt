AGS Awards 2017 Client

This client was used for the AGS Awards 2017 Ceremony,
which was held on 24. March, 2018.

You can still run the client and enter the online event hall, but
since it was a one-time, live event, there won't be much interesting
to do in there.

Note that this version of the client will not work for any future
awards ceremonies, and may at some point no longer be able to connect.

A recording of the event made by Gurok is available on YouTube:
https://www.youtube.com/watch?v=Nkyi5GoyGuE

A recording of a livestream hosted by SilverSpook is also available:
https://www.youtube.com/watch?v=IQmDf0pUtN4

The awards outcomes are available here:
http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2017

The discussion thread is here:
http://www.adventuregamestudio.co.uk/forums/index.php?topic=55659.0;all